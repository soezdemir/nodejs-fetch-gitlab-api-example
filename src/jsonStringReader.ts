const jsonString = '[{"ID": 1, "name": "Object 51", "otherProperty": "dev"}, {"ID": 3, "name": "Object 20", "otherProperty": "test"}, {"ID": 2, "name": "Object 30", "otherProperty": "backend"}]';
const jsonArray = JSON.parse(jsonString);
// Keep in mind that this example assumes you have the JSON array 
// already parsed into a JavaScript array of objects. 
// If you're starting with a JSON string, you would first need to 
// parse it using JSON.parse():

// Your JSON array containing objects
// const jsonArray = [
//     { "ID": 1, "name": "Object 1", "otherProperty": "value1" },
//     { "ID": 2, "name": "Object 2", "otherProperty": "value2" },
//     { "ID": 3, "name": "Object 3", "otherProperty": "value3" }
//   ];
  
  // Iterate over each object in the array and extract the "ID" property
  const idArray = jsonArray.map((obj: { ID: any; }) => obj.ID);
  const nameArray = jsonArray.map((obj: { name: any; }) => obj.name);
  const propertyArray = jsonArray.map((obj: { otherProperty: any; }) => obj.otherProperty);
  
  console.log(idArray, nameArray, propertyArray); // This will output: [1, 2, 3]

  // Sort the array based on the "name" property
const sortedArray = jsonArray.sort((a: { name: string; }, b: { name: any; }) => a.name.localeCompare(b.name));
console.log(sortedArray);


try {
  const jsonArray = JSON.parse(jsonString);
  console.log(jsonArray);
} catch (error) {
  console.error('Error parsing JSON:', error);
}

jsonArray.sort((a: { name: number; }, b: { name: number; }) => a.name - b.name);
// Output each object in the two-dimensional JSON object array
jsonArray.forEach((obj: { ID: any; name: any; otherProperty: any; }) => {
  console.log(`ID: ${obj.ID}, Name: ${obj.name}, otherProperty: ${obj.otherProperty}`);
});