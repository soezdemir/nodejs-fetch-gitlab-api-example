const jsonHandler = require ('./readAndSort');

const jjsonObject = {
    "ID3": 789,
    "ID1": 123,
    "ID2": 456
  };

// The main part of the code is wrapped in an immediately 
// invoked asynchronous function expression (IIFE) to 
// allow the use of await.
async function sortAndLogObject(jjsonObject: { [x: string]: any; }) {
    const sortedKeys = Object.keys(jjsonObject).sort((a, b) => jjsonObject[a] - jjsonObject[b]);
  
    for (const key of sortedKeys) {
      console.log(`${key}: ${jjsonObject[key]}`);
    }
  }
  
  
  (async () => {
    try {
      await sortAndLogObject(jjsonObject);
    } catch (error) {
      console.error('Error:', error);
    }
  })();

jsonHandler.readAndSortJson('data.json');





 
  