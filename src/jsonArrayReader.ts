const JsonArray = [
    {
      "ID": 1,
      "web_url": "https://example.com/object5",
      "name": "Object 5",
      "path_of_namespace": "namespace1",
      "otherProperty": "value1"
    },
    {
      "ID": 2,
      "web_url": "https://example.com/object3",
      "name": "Object 3",
      "path_of_namespace": "namespace2",
      "otherProperty": "value2"
    },
    {
      "ID": 3,
      "web_url": "https://example.com/object2",
      "name": "Object 2",
      "path_of_namespace": "namespace3",
      "otherProperty": "value3"
    },
    {
        "ID": 4,
        "web_url": "https://example.com/object4",
        "name": "Object 1",
        "path_of_namespace": "namespace3",
        "otherProperty": "value3"
      }
  ];
  
  // Extract desired properties and create a new array
  const selectedPropertiesArray = JsonArray.map(obj => ({
    web_url: obj.web_url,
    name: obj.name,
    path_of_namespace: obj.path_of_namespace
  }));
  
  //console.log(selectedPropertiesArray);


// Sort the array of selected properties by the "name" property
const sortedSelectedPropertiesArray = selectedPropertiesArray.sort((a, b) => a.name.localeCompare(b.name));

console.log(sortedSelectedPropertiesArray);




  