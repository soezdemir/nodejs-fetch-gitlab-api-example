interface JsonObject {
  ID1: number;
  ID2: number;
  ID3: number;
  ID4: number;
  ID5: number;
  // ... add other keys if necessary
}


const jsonObject: { [key: string]: number } = {
    "ID1": 123,
    "ID2": 456,
    "ID3": 789,
    "ID4": 678,
    "ID5": 567
  };
  
  const keyToAccess: keyof JsonObject = "ID5";
  
  console.log(jsonObject[keyToAccess as keyof typeof jsonObject]);
  console.log(jsonObject[keyToAccess]);

  // Iterate over the keys of the JSON object using Object.keys()
  Object.keys(jsonObject).forEach(key => {
    if (key.startsWith("ID")) {
      console.log(`${key}: ${jsonObject[key]}`);
    }
  });

  const sortedKeys = Object.keys(jsonObject).sort(); // Sort the keys
  sortedKeys.forEach(key => {
    console.log(`${key}: ${jsonObject[key]}`);
  });

// Sort Key Value Pairs
const keyValuePairs = Object.keys(jsonObject).map(key => ({ key, value: jsonObject[key] }));

// Sort the array of key-value pairs by value
keyValuePairs.sort((a, b) => a.value - b.value);

// Output each key-value pair in the sorted array
keyValuePairs.forEach(pair => {
  console.log(`${pair.key}: ${pair.value}`);
});
  
  