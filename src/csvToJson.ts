
const csv = require('csv-parser');
const fss = require('fs');

const csvJsonArray: any[] = [];

fss.createReadStream('data.csv')
  .pipe(csv())
  .on('data', (row: any) => {
    csvJsonArray.push(row);
  })
  .on('end', () => {
    console.log(csvJsonArray);
  });
