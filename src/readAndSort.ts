const ffs = require('fs');

interface MyObject {
    [key: string]: number; // Adjust this type to match your actual object structure
  }

export async function readAndSortJson(filePath: any) {
  return new Promise((resolve, reject) => {
    ffs.readFile(filePath, 'utf8', (err: any, data: string) => {
      if (err) {
        reject(err);
        return;
      } else {      

      //const jsonObject = JSON.parse(data);
      const jsonObject: MyObject = JSON.parse(data);

      // Sort the keys by value
      const sortedKeys = Object.keys(jsonObject).sort((a, b) => jsonObject[a] - jsonObject[b]);

      // Create a sorted object
      const sortedObject: MyObject = {};
      sortedKeys.forEach(key => {
      sortedObject[key] = jsonObject[key];
      });
    
      resolve(sortedObject);
    }
    });
  });
}

console.log(readAndSortJson('./data.json'));

module.exports = readAndSortJson;
