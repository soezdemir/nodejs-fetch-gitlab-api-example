const fs = require('fs');

// Read the JSON file
fs.readFile('data.json', 'utf8', (err: any, data: string) => {
  if (err) {
    console.error('Error reading file:', err);
    return;
  }

  const jsonArray = JSON.parse(data);

  // Output each object in the JSON array
  jsonArray.forEach((obj: { ID: any; name: any; }) => {
    console.log(`ID: ${obj.ID}, Name: ${obj.name}`);
  });
});
