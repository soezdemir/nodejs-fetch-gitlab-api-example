// const jsonArray = [
//     { "ID": 1, "name": "Object 1" },
//     { "ID": 2, "name": "Object 2" },
//     { "ID": 3, "name": "Object 3" }
//   ];
  
//   const searchString = "Object 2"; // Replace with your search string
  
//   jsonArray.forEach((obj: { name: string; ID: any; }) => {
//     if (obj.name === searchString) {
//       console.log(`Found match: ID: ${obj.ID}, Name: ${obj.name}`);
//     }
//   });



const fsss = require('fs');
const csvv = require('csv-parser');

const JSONArray: { name: string; ID: any; } [] = [];
const searchString = "MyItem"; // Replace with your search string

fsss.createReadStream('data.csv', { encoding: 'utf8' })
  .pipe(csvv({ separator: ';' }))
  .on('data', (row: any) => {
    JSONArray.push(row);
  })
  .on('end', () => {
    JSONArray.forEach((obj: { name: string; ID: any; }) => {
      if (obj.name === searchString) {
        console.log(`|---- Found match:\n ID: ${obj.ID}, Name: ${obj.name}`);
      }
    });
  });
  