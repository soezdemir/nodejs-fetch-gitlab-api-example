const apiUrl = 'https://gitlab.com/api/v4/projects'; // Replace with your GitLab API endpoint
const accessToken = 'YOUR_PRIVATE_ACCESS_TOKEN'; // Replace with your actual GitLab access token

const headers = {
  'Authorization': `Bearer ${accessToken}`
};

// Make a GET request using the Fetch API with the access token in the header
fetch(apiUrl, { headers })
  .then(response => {
    if (!response.ok) {
      throw new Error(`Network response was not ok: ${response.status} ${response.statusText}`);
    }
    return response.json(); // Parse the JSON response
  })
  .then(data => {
    console.log(data); // This will log the JSON response data
    // You can perform further processing on the data here
  })
  .catch(error => {
    console.error('Fetch error:', error);
  });
